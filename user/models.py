from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models

from user.managers import CustomUserManager


class User(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=75, default="" )
    last_name = models.CharField(max_length=75, default="")
    phone_number = models.CharField(max_length=75, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'phone_number'
    objects = CustomUserManager()

    class Meta:
        permissions = [
            ('can_see_report', 'User can see report'),
        ]

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

