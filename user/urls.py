from django.urls import path

from user.views.auth import MyTokenObtainPairView

urlpatterns = [
    path('login', MyTokenObtainPairView.as_view()),
]
