from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import Permission


class CustomUserManager(BaseUserManager):

    def create_user(self, password, **extra_fields):
        user = self.model(**extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        user = self.model(
            **extra_fields,
        )
        user.set_password(password)
        user.save(using=self._db)
        permission = Permission.objects.get(codename='can_see_report')
        user.user_permissions.add(permission)
        return user
