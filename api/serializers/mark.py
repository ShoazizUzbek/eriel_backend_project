from rest_framework import serializers


class MarkSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    value = serializers.IntegerField()
    student_id = serializers.IntegerField()
    subject_id = serializers.IntegerField()
