from rest_framework import serializers


class GroupSubjectMarkSerializer(serializers.Serializer):
    avg_subject_mark = serializers.CharField()
    group_name = serializers.CharField()
    subject_name = serializers.CharField()
