from django.db import models

# Create your models here.
from common.models import BaseModel


class Group(BaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'groups'


class Student(BaseModel):
    first_name = models.CharField(max_length=255,)
    last_name = models.CharField(max_length=255, default='last name')
    surname = models.CharField(max_length=255, default='surname')
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='students')

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name} {self.surname}'

    def __str__(self):
        return f'{self.first_name} - {self.group.name}'

    class Meta:
        db_table = 'students'


class Subject(BaseModel):
    name = models.CharField(max_length=255, )

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'subjects'


class Mark(BaseModel):
    value = models.IntegerField()
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='subject_marks',)
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='student_marks', )

    class Meta:
        db_table = 'marks'
