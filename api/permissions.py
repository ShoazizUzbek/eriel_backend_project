from rest_framework.permissions import BasePermission, IsAuthenticated


class ReportPermission(IsAuthenticated, ):
    message = 'PERMISSION_DENIED'

    def has_permission(self, request, view):
        user = request.user
        if user.has_perm('can_see_report'):
            return True
        else:
            return False