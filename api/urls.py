from django.urls import path

from api.views.mark import CreateMarkView, ListMarkView, MarkDetail
from api.views.report import StudentAverageMarkView, GroupSubjectAverageMarkView
from api.views.student import ListStudentView
from api.views.subject import ListSubjectView

urlpatterns = [
    path('student/<int:student_id>/mark/report', StudentAverageMarkView.as_view()),
    path('group/subject/mark/report', GroupSubjectAverageMarkView.as_view()),
    path('create/mark', CreateMarkView.as_view()),
    path('marks', ListMarkView.as_view()),
    path('marks/<int:pk>', MarkDetail.as_view()),
    path('subjects', ListSubjectView.as_view()),
    path('students', ListStudentView.as_view()),

]