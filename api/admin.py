from django.contrib import admin

# Register your models here.
from api.models import Group, Student, Subject, Mark


class GroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at')
    readonly_fields = ('created_at',)
admin.site.register(Group, GroupAdmin)


class StudentAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'created_at')
    readonly_fields = ('created_at',)
admin.site.register(Student, StudentAdmin)


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('name', )
    readonly_fields = ('created_at',)
admin.site.register(Subject, SubjectAdmin)


class MarkAdmin(admin.ModelAdmin):
    list_display = ('value', 'subject', 'student',)
    readonly_fields = ('created_at',)
admin.site.register(Mark, MarkAdmin)
