from django.db.models import F
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Mark
from api.serializers.mark import MarkSerializer


class CreateMarkView(APIView):
    permission_classes = (IsAuthenticated, )

    @swagger_auto_schema(request_body=MarkSerializer)
    def post(self, request):
        serializer = MarkSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        Mark.objects.create(**validated_data)
        return Response(data={
            'data': 'Mark added successfully'
        })


class ListMarkView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        marks = Mark.objects.select_related('student__group', 'subject').all().values(
            'id', 'subject_id', 'student_id', 'value',
            subject_name=F('subject__name'),
            student_name=F('student__first_name'),
            group_name=F('student__group__name')
        )
        return Response(data=marks)


class MarkDetail(APIView):

    def get(self, request, pk):
        mark = Mark.objects.select_related('student__group', 'subject').get(pk=pk)
        data = {
            'id': mark.id,
            'subject_id': mark.subject_id,
            'student_id': mark.student_id,
            'value': mark.value,
            'subject_name': mark.subject.name,
            'student_name': mark.student.first_name,
            'group_name': mark.student.group.name,
        }
        return Response(data=data)

    def put(self, request, pk):
        serializer = MarkSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        Mark.objects.filter(pk=pk).update(**validated_data)
        return Response(data={
            'data': 'Mark updated successfully'
        })

    def delete(self, request, pk):
        Mark.objects.get(pk=pk).delete()
        return Response(data={
            'data': 'Mark removed successfully'
        })
