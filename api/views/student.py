from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Student


class ListStudentView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        students = Student.objects.all().values('id', 'first_name', 'last_name')
        return Response(data=students)