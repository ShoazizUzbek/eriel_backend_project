from django.db.models import Avg, F
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Mark
from api.permissions import ReportPermission
from api.serializers.report import GroupSubjectMarkSerializer


class StudentAverageMarkView(APIView):
    permission_classes = (ReportPermission,)

    def get(self, request, student_id):
        student_marks = Mark.objects.values('subject_id').filter(
            student_id=student_id
        ).annotate(
            avg_subject_mark=Avg('value'),
            first_name=F('student__first_name'),
            last_name=F('student__last_name'),
            surname=F('student__surname'),
            group_name=F('student__group__name'),
            subject_name=F('subject__name')
        )
        return Response(data=student_marks)


class GroupSubjectAverageMarkView(APIView):
    permission_classes = (ReportPermission, )

    def get(self, request):
        group_marks = Mark.objects.values('subject_id', 'student__group_id').annotate(
            avg_subject_mark=Avg('value'),
            group_name=F('student__group__name'),
            subject_name=F('subject__name')
        )
        '''
        SELECT "marks"."subject_id", "students"."group_id", AVG("marks"."value") AS "avg_subject_mark", "groups"."name" AS "group_name", "subjects"."name" AS "subject_name" FROM "marks" INNER JOIN "subjects" ON ("marks"."subject_id" = "subjects"."id") INNER JOIN "students" ON ("marks"."student_id" = "students"."id") INNER JOIN "groups" ON ("students"."group_id" = "groups"."id") GROUP BY "marks"."subject_id", "students"."group_id", "groups"."name", "subjects"."name"'
        '''
        return Response(data=group_marks)