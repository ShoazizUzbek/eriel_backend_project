from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

#
# class CreateSubjectView(APIView):
#
#     def post(self, request):
#         serializer = MarkSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         validated_data = serializer.validated_data
#         Mark.objects.create(**validated_data)
#         return Response(data={
#             'data': 'Mark added successfully'
#         })
from api.models import Subject


class ListSubjectView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        subjects = Subject.objects.all().values('id', 'name')
        return Response(data=subjects)


